interface DbDetails {
  username: string;
  password: string;
  database: string;
  host: string;
  dialect: string;
}
interface Dbs {
    development: DbDetails;
}

const db: Dbs = {
  development : {
    username: "root",
    password: "root",
    database: "crm",
    host: "127.0.0.1",
    dialect: "mysql"
  }
};

/*const db: Dbs = {
    development : {
        username: "mis_crm_user",
        password: "m1scrmus3r123!@#",
        database: "mis_crm",
        host: "180.149.240.168",
        dialect: "mssql",
        port: 2000
    }
};*/

export default db;
