import { IEnvironmentConfiguration } from './environment';

export class EnvironmentConfiguration {
    public local() {
      const local: IEnvironmentConfiguration = {
          host: 'http://localhost:3000/',
          jwt_secret: 'asdfghjkl',
          is_production: false,
          salt: 'GYQ41CUa5UNGe9FW',
          applicationSecret: 'portal',
          applicationAdminSecret: 'adminportal',
          mailConfig: {
              hostConfig: {
                  host: '',
                  port: 25,
                  secure: true, // true for 465, false for other ports
                  auth: {
                      username: '', // generated ethereal user
                      password: '', // generated ethereal password
                  },
              },
              mailSender: '',
              mailOption: {
                  fromEmail: '', // sender address
                  toEmail: '', // list of receivers
              },
          },
          swagger_URL: '',

          otpConfig: {
              authKey: '262423AnRNIW695c626dd3',
              senderId: 'ASHIRV',
              otpSms: 'Your verification code is {{otp}}',
              otpExpiry: 0 //in minutes, keep 0 for default value
          }
      };
      return local;
    }

    public staging() {
        const staging: IEnvironmentConfiguration = {
            host: 'http://localhost:3000/',
            jwt_secret: 'asdfghjkl',
            is_production: false,
            salt: 'GYQ41CUa5UNGe9FW',
            applicationSecret: 'portal',
            applicationAdminSecret: 'adminportal',
            mailConfig: {
                hostConfig: {
                    host: '',
                    port: 25,
                    secure: true, // true for 465, false for other ports
                    auth: {
                        username: '', // generated ethereal user
                        password: '', // generated ethereal password
                    },
                },
                mailSender: '',
                mailOption: {
                    fromEmail: '', // sender address
                    toEmail: '', // list of receivers
                },
            },
            swagger_URL: '',

            otpConfig: {
                authKey: '262423AnRNIW695c626dd3',
                senderId: 'ASHIRV',
                otpSms: 'Your verification code is {{otp}}',
                otpExpiry: 0 //in minutes, keep 0 for default value
            }
        };
        return staging;
    }
}
