import {EnvironmentConfiguration} from "./EnvironmentConfiguration";

export class Environment {
    public currentEnvironment(): string {
        let apEnv: string = process.env.NODE_ENV || 'dev';
        if (!apEnv) {
            apEnv = 'local';
        }
        switch (apEnv) {
            case 'dev':
                return 'local';
            case 'staging':
                return 'staging';
            default:
                return 'local';
        }
    }

    public getConfiguration(): IEnvironmentConfiguration {
        const currentEnvironment: string = this.currentEnvironment();
        const environment: EnvironmentConfiguration = new EnvironmentConfiguration();
        switch (currentEnvironment) {
            case 'local':
                return environment.local();
            default:
                return null;
        }
    }
}

export interface IEnvironmentConfiguration {
    host: string;
    jwt_secret: string;
    is_production: boolean;
    salt: string,
    applicationSecret?: string;
    applicationAdminSecret?: string;
    mailConfig: IMailConfig;
    swagger_URL: string;

    otpConfig: IOtpConfig;
}

export interface IMailConfig {
    mailOption: IMailOption;
    mailSender?: string;
    hostConfig: IMailHostConfig;
}

export interface IMailOption {
    fromEmail: string;
    toEmail: string;
}

export interface IMailAuth {
    username: string;
    password: string;
}

export interface IMailHostConfig {
    host: string;
    port: number;
    secure: boolean;
    auth: IMailAuth;
}

//Config for MSG91
export interface IOtpConfig {
    authKey: string,
    senderId: string,
    otpSms: string,
    otpExpiry: number //in minutes
}