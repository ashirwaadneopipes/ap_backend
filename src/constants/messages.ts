/**
 * Messages
 */
export class Messages {
    public static readonly AUTHORIZATION_TOKEN_NOT_SET: string = "Authorization token not set.";
    public static readonly AUTHORIZATION_CODE_EXPIRE: string = "This authorization code has been expired.";
    public static readonly VALIDATE_AUTHORIZATION: string = "This authorization code validate successfully.";
    public static readonly INVALID_CREDENTIALS: string = "Invalid credentials.";
    public static readonly USER_FETCHED_SUCCESSFULLY: string = "User details fetched successfully.";
    public static readonly INVALID_USER: string = "User details not found.";
    public static readonly TOKEN_ERROR: string = "Token not generated.";
    public static readonly TOKEN_SUCCESS: string = "Token generated successfully.";
    public static readonly INVALID_SECRET: string = "Invalid secret.";
    public static readonly VALIDATION_ERROR: string = "Validation Error.";


    public static readonly SUCCESS: string = "SUCCESS.";

    public static readonly SUCCESSFULLY_RECORD_ADDED: string = "Data successfully inserted.";
    public static readonly SUCCESSFULLY_RECORD_UPDATED: string = "Data successfully updated.";
    public static readonly SUCCESSFULLY_RECEIVED: string = "Successfully received.";


    public static readonly INVALID_UPDATING: string = "Data not found to updated.";
    public static readonly INVALID_DELETE: string = "Data for delete not proper.";
    public static readonly INVALID_UPDATE: string = "Data for update not proper.";

    public static readonly SUCCESSFULLY_OTP_SEND: string = "OTP send successfully";
    public static readonly SUCCESSFULLY_OTP_RESEND: string = "OTP resend successfully";
    public static readonly SUCCESSFULLY_OTP_VERIFIED: string = "OTP verified successfully";
    public static readonly OTP_VERIFICATION_FAILED: string = "OTP verification failed";


    public static readonly QR_CODE_INVALID: string = "INVALID QR CODE";



}