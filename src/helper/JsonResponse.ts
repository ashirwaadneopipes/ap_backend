import {json, Request, Response} from "express";
import * as HttpStatus from 'http-status-codes';
import {ISuccessResponse} from "./IApiResponse"



export class JsonResponse{

     /**
     * jsonSuccess
     */
    public static jsonSuccess(req: Request, res: Response , method_name?:string) : void {
console.log(res.locals.success);
        const obj:ISuccessResponse = {
            success: (res.locals.success === false) ? false : true,
            data: res.locals.data,
            message: res.locals.message || "..."
        };
        res.status(HttpStatus.OK).send(obj)
    }

    /**
     * jsonError
     */
    public static jsonError(req: Request, res: Response , method_name?:string) : void {
        const obj:ISuccessResponse = {
            success: false,
            data: res.locals.data,
            message: res.locals.message || "Something went wrong, please contact to admin.",
        };
        if(!res.locals.status_code){
            res.status(HttpStatus.BAD_REQUEST);
        }else{
            res.status(res.locals.status_code);
        }
        res.send(obj);
    }
}