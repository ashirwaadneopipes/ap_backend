import {json, Request, Response} from "express";
import {JsonResponse} from "./JsonResponse";
import  *  as  multerr  from  "multer" ;
import * as fs from "fs";

const base64Img = require('base64-img');


export class Upload{
    /**
     *
     */
    public static uploadFile(upload_path: string = "/uploads")  {
        //const uploads: any
        try {
        const storage =  multerr.diskStorage ({
            destination : function ( req , file , cb ) {
                //console.log(req . file . filename);
                cb ( null , __dirname+"../../../public"+upload_path );
            },
            filename : function ( req , file , cb ) {
                cb ( null ,Date.now()+'_'+file.originalname );
            }
        });
        return  multerr ({storage: storage });
        }
        catch(error) {
            console . log (error);
        }
    }

    /**
     *
     */
    public static async uploadBase64File(upload_path: string = "/uploads",image_in_base64: any)  {
        const base = image_in_base64;
        const convert_img = base.split(';base64,').pop();
        const path:string = __dirname+"/../../public"+upload_path+"/";
        const file_name:number = Date.now();
        await fs.writeFileSync(path+file_name+'.jpg',convert_img,{encoding: 'base64'});
        return(file_name+'.jpg');
    }

    /**
     *
     */
    public static async uploadBase64FileMw(req: Request, res: Response,next:Function)  {
        const base = req.body.question_image;
        if(base) {
            const convert_img = base.split(';base64,').pop();
            let path: string = __dirname + "/../../public/uploads";
            if (req.baseUrl == "/api/question") {
                path = __dirname + "/../../public/uploads/question/";
            }
            // console.log(req.baseUrl);
            const file_name: number = Date.now();
            await fs.writeFileSync(path + file_name + '.jpg', convert_img, {encoding: 'base64'});
            req.body.question_image = file_name + '.jpg';
        }else {
            req.body.question_image = '';
        }
        next();
    }

}