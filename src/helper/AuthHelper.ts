import {NextFunction, Request, Response} from "express";
import * as express from "express";
import * as jwt from "jsonwebtoken";
import {IAuthenticationToken, IJWToken} from "./IAuth";
import * as HttpStatus from 'http-status-codes';

import {Messages} from "../constants/messages";
import {ISuccessAuthResponse} from "./IApiResponse";
import {JsonResponse} from "./JsonResponse";
import {IUser} from "../modules/user/user.types";
import {Environment} from "../../config/environments/environment";


/**
 * Authentication Helper
 */
export class AuthHelper {

    public app: express.Application;
    constructor() {
        this.app = express();
    }
    /**
     * This is middleware to validate api based on JWT token for authentication
     */
    public static async guard(req: Request, res: Response, next: NextFunction ): Promise<void>{
        try {
            const token: string = req.headers.authorization;
            if (token) {
                const authHelper: AuthHelper = new AuthHelper();
                const auth: ISuccessAuthResponse = await authHelper.validateToken(token);
                if (!auth.success) {
                    res.locals.data = {
                        isVaild: false,
                        authorizationFailed: true
                    };
                    res.locals.message = auth.message;
                    JsonResponse.jsonSuccess(req, res, 'guard');
                } else {
                    req.body.logginedUser = auth.data;
                    //console.log("here",auth.data);
                    next();
                }
            } else {
                res.locals.data = {
                    isVaild: false,
                    authorizationFailed: true
                };
                res.locals.message = Messages.AUTHORIZATION_TOKEN_NOT_SET;
                JsonResponse.jsonSuccess(req, res, 'guard');
            }
        } catch (err) {
                res.locals.data = {
                    isVaild: false,
                    authorizationFailed: true
                };
                res.locals.status_code = HttpStatus.OK;
                res.locals.message = err.message;
                JsonResponse.jsonError(req, res);
        }
    }

    /**
     * This is to validate authentication
     * { encodedToken }
     */
    public async validateToken(encodedToken: string): Promise<ISuccessAuthResponse> {
        if (!encodedToken) {
            return {
                success: false,
                message: Messages.AUTHORIZATION_CODE_EXPIRE,
                data: null
            };
        }
        const env: Environment = new Environment();
        const secret: string = env.getConfiguration().applicationAdminSecret;
        const token: IAuthenticationToken = <IAuthenticationToken>jwt.verify(encodedToken, secret);
        const authHelper: AuthHelper = new AuthHelper();
        //console.log(token);
        return authHelper.authenticateToken(token);
    }

    /**
     * This is to authenticate Token
     * { token }
     */
    public async authenticateToken(token: IAuthenticationToken): Promise<ISuccessAuthResponse> {
        return {
            success: true,
            message: Messages.SUCCESSFULLY_RECEIVED,
            data: token,
        };
    }

    /**
     * This is to generate token when login to use in API
     */
    public async generateAuthToken(user: IUser) : Promise<string> {
        const token: IAuthenticationToken = {
            user_id: user._id,
            dateIssued: new Date(),
            user_email: user.email,
            user_firstname: user.first_name,
            user_lastname: user.last_name,
        };
        //console.log(token);
        const env: Environment = new Environment();
        const secret: string = env.getConfiguration().applicationAdminSecret;
        const jwt_token: any = jwt.sign(token, secret, {expiresIn: this.daysInSeconds(6000/(24*60))});
        return jwt_token;
    }

    /**
     * To calculate days in milliseoconds
     * { days }
     */
    public daysInSeconds(days: number): number {
        // const oneMinute: number = 1000 * 60;
        const oneHour: number = 60 * 60;
        const oneDay: number = oneHour * 24;

        return oneDay * days;
    }

    /**
     * This is middleware to validate api based on JWT token
     */
    public static async getJWToken(req: Request, res: Response, next: NextFunction ): Promise<void>{
        try {
            const user_secret: string = req.body.user_secret;
            const env: Environment = new Environment();
            const secret_jwt: string = env.getConfiguration().jwt_secret;
            const application_secret: string = env.getConfiguration().applicationSecret;
            if (user_secret === secret_jwt) {
                const authHelper: AuthHelper = new AuthHelper();
                const token: string = await authHelper.generateJWTToken(application_secret);
                if (!token) {
                    res.locals.data = {
                        isVaild: false
                    };
                    res.locals.message = Messages.TOKEN_ERROR;
                    JsonResponse.jsonSuccess(req, res, 'getJWToken');
                } else {
                    res.locals.data = {
                        token: token
                    };
                    res.locals.message = Messages.TOKEN_SUCCESS;
                    JsonResponse.jsonSuccess(req, res, 'getJWToken');
                }
            } else {
                res.locals.data = {
                    isVaild: false
                };
                res.locals.message = Messages.INVALID_SECRET;
                JsonResponse.jsonSuccess(req, res, 'getJWToken');
            }
        } catch (err) {
            res.locals.data = {
                isVaild: false
            };
            res.locals.message = err.message;
            JsonResponse.jsonError(req, res);
        }
    }

    /**
     * This is to generate token for without login process
     */
    public async generateJWTToken(jwt_secret: string) : Promise<string> {
        const token: IJWToken = {
            jwt_id: jwt_secret
        };
        const env: Environment = new Environment();
        const secret: string = env.getConfiguration().applicationSecret;
        const jwt_token: any = jwt.sign(token, secret, {expiresIn: this.daysInSeconds(10/(24*60))});
        return jwt_token;
    }

    /**
     * This is middleware to validate api based on JWT token
     */
    /*public async guardJWT(req: Request, res: Response, next: NextFunction ): Promise<void>{
        try {
            const token: string = req.headers.authorization;
            if (token) {
                const authHelper: AuthHelper = new AuthHelper();
                const auth: ISuccessAuthResponse = await authHelper.validateJWTToken(token);
                if (!auth.success) {
                    res.locals.data = {
                        isVaild: false
                    };
                    res.locals.message = auth.message;
                    JsonResponse.jsonSuccess(req, res, 'guard');
                } else {
                    next();
                }
            } else {
                res.locals.data = {
                    isVaild: false
                };
                res.locals.message = Messages.INVALID_CREDENTIALS;
                JsonResponse.jsonSuccess(req, res, 'guard');
            }
        } catch (err) {
            res.locals.data = {
                isVaild: false
            };
            res.locals.message = err.message;
            JsonResponse.jsonError(req, res);
        }
    }*/

    /**
     * This is to validate JWT
     * { encodedToken }
     */
    public async validateJWTToken(encodedToken: string): Promise<ISuccessAuthResponse> {
        if (!encodedToken) {
            return {
                success: false,
                message: Messages.AUTHORIZATION_CODE_EXPIRE,
                data: null
            };
        }
        const env: Environment = new Environment();
        const secret: string = env.getConfiguration().applicationSecret;
        const token: IAuthenticationToken = <IAuthenticationToken>jwt.verify(encodedToken, secret);
        const authHelper: AuthHelper = new AuthHelper();

        return authHelper.authenticateJWTToken(token);
    }

    /**
     * This is to authenticate Token
     * { token }
     */
    public async authenticateJWTToken(token: IAuthenticationToken): Promise<ISuccessAuthResponse> {
        return {
            success: true,
            message: Messages.SUCCESSFULLY_RECEIVED,
            data: token,
        };
    }
}