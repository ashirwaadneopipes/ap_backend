export interface ISuccessResponse{
    success: boolean;
    message?: string;
    data: object;
}

export interface ISuccessAuthResponse{
    success: boolean;
    message?: string;
    data: object;
}

export interface ISuccessValidation{
    success: boolean;
    message?: string;
    data: object;
}