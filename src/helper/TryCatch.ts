import {json, Request, Response} from "express";
import {JsonResponse} from "./JsonResponse";


export class TryCatch{
    /**
     * tryCatchGlobe
     */
    public static tryCatchGlobe(handler:Function)  {
        return async (req: Request, res: Response,next:Function) => {
            try {
             await handler(req,res);                
            } catch (error) {
                res.locals.data = {
                    isVaild:false
                };
                res.locals.message = error;
                JsonResponse.jsonError(req, res);
                next(error);
            }
        }
    }
}