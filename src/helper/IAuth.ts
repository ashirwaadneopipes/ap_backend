import {IUser} from "../modules/user/user.types";

export interface ITokenValidationResponse {
    user?: IUser;
    error?: any;
    message?: string;
    status?: boolean
}

export interface IAuthenticationToken {
    user_id?: string;
    dateIssued?: Date;
    user_email?: string;
    user_firstname?: string;
    user_lastname?: string;
}

export interface IJWToken {
    jwt_id?: string;
}