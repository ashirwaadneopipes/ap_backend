import * as bodyParser from "body-parser";
import * as express from "express";
import * as path from "path";
import { registerRoutes } from "./routes";
import * as cookieParser from "cookie-parser";
import * as mongoose from "mongoose";
// import * as cors from "cors";

class App {

    public app: express.Application;
    constructor() {
        this.app = express();
        this.middleware();
        this.connectToMongo();
        this.setupRoutes();
        this.setupSwaggerDocs();
    }

    /**
     * Add all middleware
     */
    private middleware(): void {
        /*const options:cors.CorsOptions = {
            allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
            credentials: true,
            methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
            origin: "*",
            preflightContinue: false
        };
        this.app.use(cors(options));*/

        this.app.use(bodyParser.json({limit: '50mb'}));
        this.app.use(bodyParser.urlencoded({ limit: '50mb',extended: true },));
        this.app.use(cookieParser());
        // console.log(__dirname + '/../public');
        this.app.use(express.static(__dirname + '/../public'));
        this.app.use((req, res, next) => {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, credentials, withCredentials");
            res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
            next();
        });
    }

    /**
     * Connect to mongo
     */
    private connectToMongo() {
        (<any>mongoose).Promise = global.Promise;
        mongoose.connect("mongodb://localhost:27017/ap").then(
            () => {             console.log("in");
                /** ready to use. The `mongoose.connect()` promise resolves to undefined. */
                },
        ).catch(err => {
            console.log("MongoDB connection error. Please make sure MongoDB is running. " + err);
            // process.exit();
        });
    }

    /**
     * Regsiter all routes
     */
    private setupRoutes(): void {
        registerRoutes(this.app);
    }

    /**
     * Setup swagger
     */
    private setupSwaggerDocs(): void {
        this.app.use('/swagger.json', (req: express.Request, res: express.Response) => {
            res.sendFile(path.resolve(`${__dirname}/../swagger/swagger.json`));
        });
        this.app.use('/swagger.yaml', (req: express.Request, res: express.Response) => {
            res.sendFile(path.resolve(`${__dirname}/../swagger/swagger.yaml`));
        });
        this.app.use('/api/docs', express.static(path.resolve(`${__dirname}/../swagger`)));
    }
}

export default new App().app;
