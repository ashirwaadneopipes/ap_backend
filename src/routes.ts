import * as express from "express";
import {AuthController} from "./modules/auth/auth.controller";
import {UserController} from "./modules/user/user.controller";
import {PointTableController} from "./modules/point-table/point-table.controller";
import {QrCodeController} from "./modules/qr-code/qr-code.controller";
import {TransactionController} from "./modules/transaction/transaction.controller";
import {MemberTypeController} from "./modules/member-type/member-type.controller";
import {CouponController} from "./modules/coupon/coupon.controller";
import {RedeemController} from "./modules/redeem/redeem.controller";
import {QuestionsController} from "./modules/questions/questions.controller";
import {CommentController} from "./modules/comment/comment.controller";

export function registerRoutes(app: express.Application): void{
   
    new AuthController().register(app);
    new UserController().register(app);
    new PointTableController().register(app);
    new QrCodeController().register(app);
    new TransactionController().register(app);
    new MemberTypeController().register(app);
    new CouponController().register(app);
    new RedeemController().register(app);
    new QuestionsController().register(app);
    new CommentController().register(app);

}