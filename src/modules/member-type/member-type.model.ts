import { Document,Schema, model } from 'mongoose';

const MemberTypeSchema = new Schema(
    {
        min_point: {
            type: Number,
            required: true
        },
        type_name: {
            type: String,
            required: true
        },
        benefits:{
            type: String
        },
        user_type:{
            type: Number,
            default: 3
        }
    },
    {
       timestamps: true,
       useNestedStrict: true
    }
);



export default model ('member_type', MemberTypeSchema);