export interface IMemberTypeTypes {
    _id?: any;
    min_point: number;
    type_name: string;
    benefits?: string;
    user_type?: number;
}