import { Request, Response, Application } from "express";
import { BaseController } from '../BaseController';
import {MemberTypeRepo} from "./member-type.repo";
import { TryCatch,JsonResponse,AuthHelper } from "../../helper";
import {Messages} from "../../constants/messages";
import {MemberTypeValidation} from "./member-type.validation";

export class MemberTypeController extends BaseController {
    constructor(){
        super(new MemberTypeRepo(),'member-type','_id');
        this.init();
    }

    public register(express: Application) : void{
        express.use('/api/manage/member-type', this.router);
    }

    public init(): void {
        const validation: MemberTypeValidation = new MemberTypeValidation();
        this.router.get('/' ,TryCatch.tryCatchGlobe(this.indexB));
        this.router.post('/', AuthHelper.guard, TryCatch.tryCatchGlobe(this.addB));
        this.router.put('/',  AuthHelper.guard, TryCatch.tryCatchGlobe(this.editB));
        this.router.delete('/', AuthHelper.guard, TryCatch.tryCatchGlobe(this.deleteByIdB));
        this.router.post('/get-by-id', AuthHelper.guard, TryCatch.tryCatchGlobe(this.getByIdB));
    }
}