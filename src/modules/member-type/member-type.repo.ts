import MemberType from "./member-type.model";
import {Messages} from "../../constants/messages";
import {BaseRepo} from "../BaseRepo";

export class MemberTypeRepo extends BaseRepo{

    constructor() {
        super(MemberType,'_id');
    }

    public async getByPartNumber(part_number:number){
        let point_data = await MemberType.findOne({
            part_number: part_number
        });
        return point_data;
    }

    public async getMemberTypeMin(user_point:number){
        console.log("inside" , user_point);
        let point_data = await MemberType.find({
            min_point: { $lte : user_point}
        }).sort("-min_point")
            .limit(1).exec();
        return point_data;
    }

    public async getMemberTypeMax(user_point:number){
        let point_data = await MemberType.find({
            min_point: { $gt : user_point}
        }).sort("min_point")
            .limit(1).exec();
        return point_data;
    }

}
