import { BaseController } from "../BaseController";
import {Application, NextFunction, Request, Response} from "express";
import {AuthHelper} from "../../helper/AuthHelper";
import {IUser, IUserResponse} from "../user/user.types";
import {UserValidation} from "../user/user.validation";
import {UserRepo} from "../user/user.repo";
import {JsonResponse} from "../../helper/JsonResponse";
import {TryCatch} from "../../helper/TryCatch";
import {Messages} from "../../constants/messages";
import {ISuccessResponse} from "../../helper/IApiResponse";

/**
 * Auth API
 */
export class AuthController extends BaseController {

    //protected UserValidation:UserValidation;


    constructor() {
        super();
        this.init();
        //this.UserValidation = new UserValidation;
    }

    /**
     * To register auth module base API
     * @param express
     */
    public register(express: Application) : void{
        express.use('/api/auth', this.router);
    }

    /**
     * Initialize the API for authentication
     * { host/api/auth }
     */
    public init(): void {
        const userValidation: UserValidation = new UserValidation();
        this.router.post('/login', userValidation.loginValidation,TryCatch.tryCatchGlobe(this.loginAction));
        this.router.get('/is-logged-in',AuthHelper.guard, TryCatch.tryCatchGlobe(this.isLoggedIn));
        this.router.post('/getJWT', TryCatch.tryCatchGlobe(AuthHelper.getJWToken));
    }

    /**
     * This is to login
     * @param req
     * @param res
     */
    public async loginAction(req: Request , res: Response, next: NextFunction): Promise<void> {
        const user: UserRepo = new UserRepo();
        const params: IUser = req.body;
        const items: IUserResponse = await user.getUsersProfile(params);

        if (items.data === null) {
            res.locals.data = {
                isValid:false
            };
            res.locals.message = Messages.INVALID_CREDENTIALS;
            return JsonResponse.jsonSuccess(req, res, "login");
        }
        const currentUser: IUser = items.data;
        const authHelper: AuthHelper = new AuthHelper();
        const token: string = await authHelper.generateAuthToken(currentUser);
        res.locals.data = {
            user_token : token,
            email: currentUser["email"],
            first_name: currentUser["first_name"],
            last_name: currentUser["last_name"],
            _id: currentUser["_id"]
        };
        JsonResponse.jsonSuccess(req,res, "login");
    }

    /**
     * This is to check logged in or not
     * @param req
     * @param res
     */
    public async isLoggedIn(req: Request, res: Response, next: NextFunction){
        res.locals.data = {
            isValid: true,
            isLoggedIn: true
        };
        JsonResponse.jsonSuccess(req,res, "isLogin");
    }
}