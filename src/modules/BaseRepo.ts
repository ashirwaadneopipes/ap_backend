export class BaseRepo {

    protected modelName: any;
    protected queryParam: string;

    protected constructor(model: any = '', query_param: string = ''){
        this.modelName = model;
        this.queryParam = query_param;
    }

    public async index(relation: object = [],select_column: any = ''): Promise<object>{
        let data = await this.modelName.find().populate('point_table').exec();
        return Promise.resolve(data);
    }

    public async add(new_data: any): Promise<object>{
        let data  = new this.modelName(new_data);
        return Promise.resolve(await data.save());
    }

    public async edit(edit_data: {[key: string]: any}): Promise<number>{
        return await this.modelName.findByIdAndUpdate(edit_data._id,{$set:edit_data}, {new: true}).exec();
    }

    public async delete(id: string): Promise<number>{
        let deleted_data = await this.modelName.findOneAndRemove({_id:id});
        return Promise.resolve(deleted_data);
    }

    public async getByID(id: number): Promise<object>{
        let data = await this.modelName.findById(id).exec();
        return Promise.resolve(data);

    }

}