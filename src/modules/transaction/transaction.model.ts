import { Document,Schema, model } from 'mongoose';

const TransactionSchema = new Schema(
    {
        user: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        },
        total_reward_points: {
            type: Number,
            required: true
        },
        type: {
            type: String,
            required: true
        },
        is_approved:{
            type: Boolean,
            default: false
        }
    },
    {
       timestamps: true,
       useNestedStrict: true
    }
);



export default model ('transaction', TransactionSchema);