import { Request, Response, Application } from "express";
import { BaseController } from '../BaseController';
import {TransactionRepo} from "./transaction.repo";
import { TryCatch,JsonResponse,AuthHelper } from "../../helper";
import {Messages} from "../../constants/messages";
import {TransactionValidation} from "./transaction.validation";

export class TransactionController extends BaseController {
    constructor(){
        super(new TransactionRepo(),'transaction','_id');
        this.init();
    }

    public register(express: Application) : void{
        express.use('/api/transaction', this.router);
    }

    public init(): void {
        const validation: TransactionValidation = new TransactionValidation();
        this.router.get('/' ,TryCatch.tryCatchGlobe(this.indexB));
        this.router.post('/', AuthHelper.guard, TryCatch.tryCatchGlobe(this.addB));
        this.router.put('/',  AuthHelper.guard, TryCatch.tryCatchGlobe(this.editB));
        this.router.delete('/', AuthHelper.guard, TryCatch.tryCatchGlobe(this.deleteByIdB));
        this.router.post('/get-by-id', AuthHelper.guard, TryCatch.tryCatchGlobe(this.getByIdB));

        this.router.get('/get-by-user', AuthHelper.guard, TryCatch.tryCatchGlobe(this.getByUser));
    }

    public async getByUser(req: Request, res: Response) {
        const new_data = req.body;
        new_data.user = req.body.logginedUser.user_id;
        const repo = new TransactionRepo();
        let all_transaction:any = await repo.getByUser(new_data.user);
        res.locals.data = all_transaction;
        res.locals.message = Messages.SUCCESSFULLY_RECORD_ADDED;
        JsonResponse.jsonSuccess(req, res, 'redeem.getAllRedeemByUser');
    }
}