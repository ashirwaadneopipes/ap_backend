export interface ITransactionTypes {
    _id?: any;
    total_reward_points: number;
    type: string;
    is_approved: boolean;
}