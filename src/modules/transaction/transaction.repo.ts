import Transaction from "./transaction.model";
import {Messages} from "../../constants/messages";
import {BaseRepo} from "../BaseRepo";

export class TransactionRepo extends BaseRepo{

    constructor() {
        super(Transaction,'_id');
    }

    public async getByPartNumber(part_number:number){
        let point_data = await Transaction.findOne({
            part_number: part_number
        });
        return point_data;
    }

    public async getByUser(user:any){
        return await Transaction.find({ "user": user }).sort("-createdAt").exec();
    }
}
