import { Document,Schema, model } from 'mongoose';

const CouponSchema = new Schema(
    {
        name:{
            type: String,
            required: true
        },
        redeem_point:{
            type:Number,
            required: true
        },
        coupon_code:{
            type:String,
            required: true
        },
        description:{
            type: String,
        },
        latitude:{
            type: String,
        },
        longitude:{
            type: String,
        }
    },
    {
       timestamps: true,
       useNestedStrict: true
    }
);



export default model ('coupon', CouponSchema);