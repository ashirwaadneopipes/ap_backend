import { Request, Response, Application } from "express";
import { BaseController } from '../BaseController';
import {CouponRepo} from "./coupon.repo";
import {PointTableRepo} from "../point-table/point-table.repo";
import {TransactionRepo} from "../transaction/transaction.repo";
import {UserRepo} from "../user/user.repo";
import {MemberTypeRepo} from "../member-type/member-type.repo";
import { TryCatch,JsonResponse,AuthHelper } from "../../helper";
import {Messages} from "../../constants/messages";
import {CouponValidation} from "./coupon.validation";
import * as _ from "lodash";
const request = require('request-json');

export class CouponController extends BaseController {
    constructor(){
        super(new CouponRepo(),'coupon','_id');
        this.init();
    }

    public register(express: Application) : void{
        express.use('/api/manage/coupon', this.router);
    }

    public init(): void {
        // const validation: CouponValidation = new CouponValidation();
        this.router.get('/' ,TryCatch.tryCatchGlobe(this.indexB));
        this.router.post('/', AuthHelper.guard, TryCatch.tryCatchGlobe(this.addB));
        this.router.put('/', AuthHelper.guard, TryCatch.tryCatchGlobe(this.editB));
        this.router.delete('/', AuthHelper.guard, TryCatch.tryCatchGlobe(this.deleteByIdB));
        this.router.post('/get-by-id', AuthHelper.guard, TryCatch.tryCatchGlobe(this.getByIdB));
    }
}