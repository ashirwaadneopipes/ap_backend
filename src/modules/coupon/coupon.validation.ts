import * as joi from "joi";
import {Request, Response, NextFunction} from "express";
import {ISuccessValidation,JsonResponse} from "../../helper";
import {Messages} from "../../constants";



export class CouponValidation {

    public async addValidation(req: Request, res: Response, next:NextFunction): Promise<void> {
        console.log(req.body);
        var bodySchema = joi.object({
            name : joi.string().required().label('This field is invalid'),
            redeem_point: joi.number().required().label('This field is invalid'),
            // user: joi.string().required().label('This field is invalid')
        });
        bodySchema.validate(req.body,{abortEarly: false},(err, value) => {
            if (err === null) {
                next();
            }else{
                res.locals.data ={
                    validation: false,
                    isValid: false,
                    validation_error: err.details,
                };
                JsonResponse.jsonSuccess(req, res);
            }
        });
    }

    public async editValidation(req: Request, res: Response, next:NextFunction): Promise<void> {
        console.log(req.body);
        var bodySchema = joi.object({
            _id : joi.string().required().label('This field is invalid'),
            name : joi.string().required().label('This field is invalid'),
            redeem_point: joi.number().required().label('This field is invalid'),
            // user: joi.string().required().label('This field is invalid'),
            // password: joi.string().required().label('This field is invalid')
        });
        bodySchema.validate(req.body,{abortEarly: false},(err, value) => {
            if (err === null) {
                next();
            }else{
                res.locals.data ={
                    validation: false,
                    isValid: false,
                    validation_error: err.details,
                };
                JsonResponse.jsonSuccess(req, res);
            }
        });
    }
}