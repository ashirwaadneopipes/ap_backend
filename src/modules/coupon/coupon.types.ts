export interface IQrCodeTypes {
    _id?: any;
    point_table: string;
    qr_code: string;
    users: number;
}