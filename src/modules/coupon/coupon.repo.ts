import Coupon from "./coupon.model";
import {Messages} from "../../constants/messages";
import {BaseRepo} from "../BaseRepo";

export class CouponRepo extends BaseRepo{

    constructor() {
        super(Coupon,'_id');
    }

}
