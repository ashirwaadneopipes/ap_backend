import { Request, Response, Application } from "express";
import { BaseController } from '../BaseController';
import {RedeemRepo} from "./redeem.repo";
import {PointTableRepo} from "../point-table/point-table.repo";
import {TransactionRepo} from "../transaction/transaction.repo";
import {UserRepo} from "../user/user.repo";
import {MemberTypeRepo} from "../member-type/member-type.repo";
import { TryCatch,JsonResponse,AuthHelper } from "../../helper";
import {Messages} from "../../constants/messages";
import {CouponValidation} from "./redeem.validation";
import * as _ from "lodash";
const request = require('request-json');

export class RedeemController extends BaseController {
    constructor(){
        super(new RedeemRepo(),'redeem','_id');
        this.init();
    }

    public register(express: Application) : void{
        express.use('/api/redeem', this.router);
    }

    public init(): void {
        // const validation: CouponValidation = new CouponValidation();
        this.router.get('/' ,TryCatch.tryCatchGlobe(this.indexB));
        this.router.post('/', AuthHelper.guard, TryCatch.tryCatchGlobe(this.addB));
        this.router.put('/', AuthHelper.guard, TryCatch.tryCatchGlobe(this.editB));
        this.router.delete('/', AuthHelper.guard, TryCatch.tryCatchGlobe(this.deleteByIdB));
        this.router.post('/get-by-id', AuthHelper.guard, TryCatch.tryCatchGlobe(this.getByIdB));

        this.router.post('/add-redeem', AuthHelper.guard, TryCatch.tryCatchGlobe(this.addRedeem))
        this.router.get('/get-by-user', AuthHelper.guard, TryCatch.tryCatchGlobe(this.getAllRedeemByUser))
    }

    public async addRedeem(req: Request, res: Response) {
        const new_data = req.body;
        new_data.user = req.body.logginedUser.user_id;
        const repo = new RedeemRepo();
        const transaction_repo = new TransactionRepo();
        const user_repo = new UserRepo();
        const member_repo = new MemberTypeRepo();
        let initial_value = 0;
        let total_redeem:number = new_data.coupon_codes.reduce(function (accumulator:any, current_value:any) {
            return accumulator + parseInt(current_value.redeem_points);
        },initial_value);

        // console.log(total_redeem);
        let transaction:any = await transaction_repo.add({
            user: new_data.user,
            total_reward_points: total_redeem,
            type: "debited",
            is_approved: false
        });

        console.log(transaction);

        let user_details:any = await user_repo.getByID(new_data.user);


        let final_coupon_code_arr:any = [];
        new_data.coupon_codes.forEach(function (coupon_code:any) {
            final_coupon_code_arr.push({
                coupon_code:coupon_code.coupon_code,
                redeem_point:coupon_code.redeem_points,
                transaction: transaction._id,
                user:user_details._id,
            });
            // return result
        });
        let all_coupons:any = await repo.addBulk(final_coupon_code_arr);
        console.log(all_coupons);

        // update user

        user_details.current_points = user_details.current_points - total_redeem;
        user_details.total_redemption = user_details.total_redemption + total_redeem;
        user_details = await user_repo.edit(user_details);

        res.locals.data = {coupon_codes:all_coupons,user_details:user_details,total_redeem:total_redeem};
        res.locals.message = Messages.SUCCESSFULLY_RECORD_ADDED;
        JsonResponse.jsonSuccess(req, res, 'qr-code.isQrValid');


    }

    public async getAllRedeemByUser(req: Request, res: Response) {
        const new_data = req.body;
        new_data.user = req.body.logginedUser.user_id;
        const repo = new RedeemRepo();
        let all_redeems:any = await repo.getByUser(new_data.user);
        res.locals.data = all_redeems;
        res.locals.message = Messages.SUCCESSFULLY_RECORD_ADDED;
        JsonResponse.jsonSuccess(req, res, 'redeem.getAllRedeemByUser');
    }

}