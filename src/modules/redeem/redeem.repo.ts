import Redeem from "./redeem.model";
import {Messages} from "../../constants/messages";
import {BaseRepo} from "../BaseRepo";

export class RedeemRepo extends BaseRepo{

    constructor() {
        super(Redeem,'_id');
    }

    public async addBulk(new_data: any){
        // console.log(new_data);
        await Redeem.insertMany(new_data,function(error, docs) {
            return docs;
        });
    }

    public async getByUser(user:any){
        /*let point_data = await PointTable.findOne({
            part_number: part_number
        });*/

        return await Redeem.find({ "user": user });
    }


}
