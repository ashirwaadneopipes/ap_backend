import { Document,Schema, model } from 'mongoose';

const RedeemSchema = new Schema(
    {
        redeem_point:{
            type:Number,
            required: true
        },
        coupon_code:{
            type:String,
            required: true
        },
        latitude:{
            type: String,
        },
        longitude:{
            type: String,
        },
        transaction: {
            type: Schema.Types.ObjectId,
            ref: 'transaction'
        },
        user: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        }
    },
    {
       timestamps: true,
       useNestedStrict: true
    }
);



export default model ('redeem', RedeemSchema);