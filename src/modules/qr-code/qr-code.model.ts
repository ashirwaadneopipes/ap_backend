import { Document,Schema, model } from 'mongoose';

const QrCodeSchema = new Schema(
    {
        qr_code: {
            type: String,
            required: true,
            trim: true
        },
        point:{
            type:Number
        },
        latitude:{
            type: String,
        },
        longitude:{
            type: String,
        },
        point_table: {
            type: Schema.Types.ObjectId,
            ref: 'point_table'
        },
        transaction: {
            type: Schema.Types.ObjectId,
            ref: 'transaction'
        },
        user: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        }
    },
    {
       timestamps: true,
       useNestedStrict: true
    }
);



export default model ('qr_code', QrCodeSchema);