import { Request, Response, Application } from "express";
import { BaseController } from '../BaseController';
import {QrCodeRepo} from "./qr-code.repo";
import {PointTableRepo} from "../point-table/point-table.repo";
import {TransactionRepo} from "../transaction/transaction.repo";
import {UserRepo} from "../user/user.repo";
import {MemberTypeRepo} from "../member-type/member-type.repo";
import { TryCatch,JsonResponse,AuthHelper } from "../../helper";
import {Messages} from "../../constants/messages";
import {QrCodeValidation} from "./qr-code.validation";
import * as _ from "lodash";
const request = require('request-json');
export class QrCodeController extends BaseController {
    constructor(){
        super(new QrCodeRepo(),'qr-code','_id');
        this.init();
    }

    public register(express: Application) : void{
        express.use('/api/qr-code', this.router);
    }

    public init(): void {
        const validation: QrCodeValidation = new QrCodeValidation();
        this.router.get('/' ,TryCatch.tryCatchGlobe(this.indexB));

        this.router.post('/', AuthHelper.guard, TryCatch.tryCatchGlobe(this.add));

        this.router.put('/', AuthHelper.guard, TryCatch.tryCatchGlobe(this.editB));
        this.router.delete('/', AuthHelper.guard, TryCatch.tryCatchGlobe(this.deleteByIdB));
        this.router.post('/get-by-id', AuthHelper.guard, TryCatch.tryCatchGlobe(this.getByIdB));

        this.router.post('/is-qr-valid', AuthHelper.guard, TryCatch.tryCatchGlobe(this.isQrValid));
        this.router.post('/get-product-type-name', AuthHelper.guard, TryCatch.tryCatchGlobe(this.getPointName));
    }

    /**
     * add
     * @param req
     * @param res
     */
    public async add(req: Request, res: Response){
        const new_data = req.body;
        new_data.user = req.body.logginedUser.user_id;
        const repo = new QrCodeRepo();
        res.locals.data = await repo.add(new_data);
        res.locals.message = Messages.SUCCESSFULLY_RECORD_ADDED;
        JsonResponse.jsonSuccess(req, res, 'qr-code.add');
    }

    public async isQrValid(req: Request, res: Response){
        const new_data = req.body;
        new_data.user = req.body.logginedUser.user_id;
        const repo = new QrCodeRepo();
        const point_repo = new PointTableRepo();
        const transaction_repo = new TransactionRepo();
        const user_repo = new UserRepo();
        const member_repo = new MemberTypeRepo();
        // console.log(req.body);


        const boasch_api = request.createClient('http://180.179.7.183/');
        let api_responses:any = await boasch_api.post('DemoAPI/api/DemoAPI/PostValidateQRCodeDemo', new_data.qr_codes);
// console.log(new_data.qr_codes);
        /*let api_responses:any = {};
         api_responses.body = [
            {
                "QR":"12345",
                "Stats":"0",
                "Message":"INVALID QR CODE LENGTH",
                "Description":"Please Check the Length of Scanned QRCode."
            },
            {
                "QR":"1234567890123456789012",
                "Stats":"0",
                "Message":"Invalid QR Code",
                "Description":"QR Code is invalid"
            },
            {
                "QR":"7000000098765432112332",
                "Stats":"0",
                "Message":"DUPLICATE QRCODE SCANNED",
                "Description":"Scanned QRCode has already been redeemed. Please Scan Another QRCode to avail Loyalty Points."
            },
            {
                "QR":"7000023619030541411406",
                "Stats":"1",
                "Message":"LOYALTY POINT CREDITED",
                "Description":"Loyalty Point Redeemed against the Scanned QR Code for this User."
            },
             {
                 "QR":"7000000019030541411406",
                 "Stats":"1",
                 "Message":"LOYALTY POINT CREDITED",
                 "Description":"Loyalty Point Redeemed against the Scanned QR Code for this User."
             }
        ];*/

        let valid_qr_code:any = [];
        const selected_qr = api_responses.body.filter(function (api_response:any) {
            if(api_response.Stats == 1){
                valid_qr_code.push(api_response.QR);
            }
            return api_response.Stats == 1;
        });

        // console.log(valid_qr_code);
        let duplicates:any = await repo.findDublicates(valid_qr_code);
        // console.log(duplicates);

        _.remove(valid_qr_code,function (el:any) {
            return _.includes(duplicates,el)
        });
        let point_detail_obj:any = {}
        if(valid_qr_code.length > 0){
            const point_table_ids:any =valid_qr_code.map(function (valid_length:any) {
               return valid_length.substring(0,8);
            });
            const point_details:any = await point_repo.getByPartNumber(point_table_ids);
            _.forEach(point_details, function(point_detail:any) {
                point_detail_obj[point_detail.part_number] = {reward_points:point_detail.reward_points,product_name:point_detail.name,point_table:point_detail._id};
            });
        }

        duplicates = _.map(duplicates, 'qr_code');
        let total_rewards: number = 0;
        let total_qrcode_valid: number = 0;
        api_responses = api_responses.body.map(function (api_response:any) {
            api_response.part_number = api_response.QR.substring(0,8);
            api_response.user = new_data.user;
            api_response.latitude = new_data.coordinate.latitude;
            api_response.longitude = new_data.coordinate.longitude;
            api_response.product_name = point_detail_obj[api_response.part_number] ? point_detail_obj[api_response.part_number].product_name : '';
            if(_.includes(duplicates,api_response.QR)){
                api_response.reward_points =  0;
                api_response.is_dublicates = true;
                api_response.Message = "LOYALTY POINT ALREADY CREDITED";
            }else{
                api_response.reward_points = (point_detail_obj[api_response.part_number] && api_response.Stats == '1') ? point_detail_obj[api_response.part_number].reward_points : 0;
                api_response.point_table = (point_detail_obj[api_response.part_number]) ? point_detail_obj[api_response.part_number].point_table : null;
                api_response.is_dublicates = false;
            }
            if(api_response.Stats == '1' && api_response.is_dublicates == false){
                total_qrcode_valid++;
                total_rewards += api_response.reward_points;
            }
            return api_response;
        });
        // console.log(total_rewards);

        if(total_rewards>0){
            let transaction:any = await transaction_repo.add({
                user: new_data.user,
                total_reward_points: total_rewards,
                type: "credited",
                is_approved: false
            });

            let final_qr_code_arr:any = [];
            api_responses.forEach(function (api_response:any) {
                // console.log(api_response);
                if(api_response.Stats == '1' && api_response.is_dublicates == false){
                    final_qr_code_arr.push({
                        qr_code:api_response.QR,
                        point:api_response.reward_points,
                        latitude:api_response.latitude,
                        longitude:api_response.longitude,
                        point_table:api_response.point_table,
                        transaction: transaction._id,
                        user:api_response.user,
                    });
                    // return result
                }
            });

            // console.log(final_qr_code_arr);
            // res.locals.data = {qr_codes:api_responses};
            // res.locals.message = Messages.SUCCESSFULLY_RECORD_ADDED;
            // JsonResponse.jsonSuccess(req, res, 'qr-code.isQrValid');
            await repo.addBulk(final_qr_code_arr);
        }



        // update user
        let user_details:any = await user_repo.getByID(new_data.user);
        let membership_min:any = await member_repo.getMemberTypeMin(user_details.total_points + total_rewards);
        let membership_max:any = await member_repo.getMemberTypeMax(user_details.total_points + total_rewards);
        console.log(user_details.total_points);

        user_details.member_type_min = membership_min[0]._id;
        user_details.member_type_max = membership_max[0]._id;
        user_details.member_type_name_min = membership_min[0].type_name;
        user_details.member_type_name_max = membership_max[0].type_name;
        user_details.current_points += total_rewards;
        user_details.total_points += total_rewards;
        let update_user:any = await user_repo.edit(user_details);

        // console.log(user_details.total_points);

        res.locals.data = {qr_codes:api_responses,user_details:update_user,total_reward:total_rewards,valid_qr:total_qrcode_valid};
        res.locals.message = Messages.SUCCESSFULLY_RECORD_ADDED;
        JsonResponse.jsonSuccess(req, res, 'qr-code.isQrValid');

        //update user

        // const point_table_ids:any = [];
        /*const duplicates:any = duplicates.map(function (valid_length:any) {
            point_table_ids.push(valid_length.QR.substring(0,8));
            return {QR: valid_length.QR , point_table: valid_length.QR.substring(0,8)};
        });*/
        // res.locals.message = Messages.SUCCESSFULLY_RECORD_ADDED;
        // JsonResponse.jsonSuccess(req, res, 'qr-code.add');
    }

    public async getPointName(req: Request, res: Response){
        const qr_codes = req.body.qr_codes;
        const point_repo = new PointTableRepo();
        const point_table_ids:any =qr_codes.map(function (valid_length:any) {
            return valid_length.QR.substring(0,8);
        });
        let point_detail_obj:any = {};
        const point_details:any = await point_repo.getByPartNumber(point_table_ids);
        _.forEach(point_details, function(point_detail:any) {
            point_detail_obj[point_detail.part_number] = point_detail.name;
        });
        let qr_codes_valid_flag: any = [];
        _.forEach(qr_codes, function(qr_code:any) {
            const part_name:any = point_detail_obj[qr_code.QR.substring(0,8)] ? point_detail_obj[qr_code.QR.substring(0,8)] : '';
            qr_codes_valid_flag.push(
                {
                    "QR":qr_code.QR,
                    product_name:part_name
                });
        });
        // return {"qr_codes":qr_codes_valid_flag};
        res.locals.data = {"qr_codes":qr_codes_valid_flag};
        res.locals.message = Messages.SUCCESS;
        JsonResponse.jsonSuccess(req, res, 'qr-code.getPointName');
    }





    /*public async isQrValid(req: Request, res: Response){
        const qr_codes:any = req.body.qr_codes;
        console.log(qr_codes);
        const repo = new QrCodeRepo();
        const point_repo = new PointTableRepo();
        /!*!//check length of qr code it should be 22
        const lg_qr_code:any = qr_code.length();
        if(lg_qr_code != 22){
            res.locals.data = {
                QR:qr_code,
                Stats:0,
                Message: "INVALID QR CODE LENGTH",
                Description: "Please Check the Length of Scanned QRCode."
            };
            res.locals.message = Messages.QR_CODE_INVALID;
            JsonResponse.jsonSuccess(req, res, 'qr-code.isQrValid');
        }
        //check length of qr code it should be 22

        //Invalid model number
        const part_number:number = parseInt(qr_code.trim().substring(0,8));
        const is_part_number_exist:any = await point_repo.getByPartNumber(part_number);
        if(is_part_number_exist === null){
            res.locals.data = {
                QR:qr_code,
                Stats:0,
                Message: "Invalid QR Code",
                Description: "Please Check the Length of Scanned QRCode."
            };
            res.locals.message = Messages.QR_CODE_INVALID;
            JsonResponse.jsonSuccess(req, res, 'qr-code.isQrValid');
        }
        //Invalid model number*!/
        //const length_true = this.validateQrCodeLength(qr_codes);

        const valid_lengths:any = qr_codes.filter(function (qr_code:any) {
            return qr_code.QR.length === 22;
        });
        // console.log(valid_length);
        const point_table_ids:any = [];
        const valid_length_point_table:any = valid_lengths.map(function (valid_length:any) {
            point_table_ids.push(valid_length.QR.substring(0,8));
            return {QR: valid_length.QR , point_table: valid_length.QR.substring(0,8)};
        });
        console.log(points);

        // res.locals.data = await repo.add(valid_length);
        res.locals.message = Messages.SUCCESSFULLY_RECORD_ADDED;
        JsonResponse.jsonSuccess(req, res, 'qr-code.add');
    }*/

    //check length of qr code it should be 22
    /*protected validateQrCodeLength(qr_codes:any){
        console.log(qr_codes);
        const valid_lenght:any = qr_codes.filter(function (qr_code:any) {
           return qr_code.QR.length === 22;
        });
        return valid_lenght;
    }*/

    //Invalid model number
    /*public async validateQrPointTable(qr_codes:any){
        qr_codes
        const part_number:number = parseInt(qr_code.trim().substring(0,8));
        const point_repo = new PointTableRepo();
        const is_part_number_exist:any = await point_repo.getByPartNumber(part_number);
        if(is_part_number_exist === null){
            return {
                QR:qr_code,
                Stats:0,
                Message: "Invalid QR Code",
                Description: "Please Check the Length of Scanned QRCode."
            }
        }else{
            return { is_part_number_exist };
        }
    }*/
}