import QrCode from "./qr-code.model";
import {Messages} from "../../constants/messages";
import {BaseRepo} from "../BaseRepo";

export class QrCodeRepo extends BaseRepo{

    constructor() {
        super(QrCode,'_id');
    }

    public async findDublicates(qr_codes:any){
        return await QrCode.find({ "qr_code": { "$in": qr_codes } },{"qr_code":1});
    }

    public async addBulk(new_data: any){
        await QrCode.insertMany(new_data,function(error, docs) {
            return docs;
        });
    }

}
