import PointTable from "./point-table.model";
import {Messages} from "../../constants/messages";
import {BaseRepo} from "../BaseRepo";

export class PointTableRepo extends BaseRepo{

    constructor() {
        super(PointTable,'_id');
    }

    public async getByPartNumber(part_numbers:any){
        /*let point_data = await PointTable.findOne({
            part_number: part_number
        });*/

        return await PointTable.find({ "part_number": { "$in": part_numbers } });
    }

}
