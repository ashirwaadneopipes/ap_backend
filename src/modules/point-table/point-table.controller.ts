import { Request, Response, Application } from "express";
import { BaseController } from '../BaseController';
import {PointTableRepo} from "./point-table.repo";
import { TryCatch,JsonResponse,AuthHelper } from "../../helper";
import {Messages} from "../../constants/messages";
import {PointTableValidation} from "./point-table.validation";

export class PointTableController extends BaseController {
    constructor(){
        super(new PointTableRepo(),'point-table','_id');
        this.init();
    }

    public register(express: Application) : void{
        express.use('/api/point-table', this.router);
    }

    public init(): void {
        const validation: PointTableValidation = new PointTableValidation();
        this.router.get('/' ,TryCatch.tryCatchGlobe(this.indexB));
        this.router.post('/',  AuthHelper.guard, TryCatch.tryCatchGlobe(this.addB));
        this.router.put('/',  AuthHelper.guard, TryCatch.tryCatchGlobe(this.editB));
        this.router.delete('/', AuthHelper.guard, TryCatch.tryCatchGlobe(this.deleteByIdB));
        this.router.post('/get-by-id', AuthHelper.guard, TryCatch.tryCatchGlobe(this.getByIdB));
    }
}