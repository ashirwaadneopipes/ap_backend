export interface IPointTableTypes {
    _id?: any;
    name: string;
    part_number: number;
    reward_points: number;
}