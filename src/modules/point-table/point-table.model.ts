import { Document,Schema, model } from 'mongoose';

const PointTableSchema = new Schema(
    {
        part_number: {
            type: Number,
            required: true,
            trim: true
        },
        name: {
            type: String,
            required: true
        },
        reward_points: {
            type: Number,
            required: true,
            trim: true
        }
    },
    {
       // timestamps: true,
        useNestedStrict: true
    }
);



export default model ('point_table', PointTableSchema);