import {Application, Router, Request, Response} from "express";
import {JsonResponse} from "../helper/JsonResponse";
import {Messages} from "../constants/";

export abstract class BaseController {

    protected router: Router;
    protected modelRepo: any;
    protected url: string;
    protected queryParam: string;

    protected constructor(model: any = '', url: string = '', query_param: string = ''){
        this.modelRepo = model;
        this.url = url;
        this.queryParam = query_param;
        this.router =  Router();
    }

    /**
     * This is to List
     * @param req
     * @param res
     */
    public async index(req: Request, res: Response): Promise<void> {
        console.log("in thereasdasdasd repo");
        res.locals.data = await this.modelRepo.index();
        res.locals.message = Messages.SUCCESSFULLY_RECEIVED;
        return JsonResponse.jsonSuccess(req, res, this.url+'.index');
    }

    /**
     * This is to List
     * @param req
     * @param res
     */
    public indexB =  async (req: Request, res: Response): Promise<void> => {
       // console.log("in thereasdasdasd");
        res.locals.data = await this.modelRepo.index();
        res.locals.message = Messages.SUCCESSFULLY_RECEIVED;
        return JsonResponse.jsonSuccess(req, res, this.url+'.index');
    }

    /**
     * add
     * @param req
     * @param res
     */
    public addB = async (req: Request, res: Response): Promise<void> => {
        const new_data = req.body;
        new_data.created_by = req.body.logginedUser.user_id;
        res.locals.data = await this.modelRepo.add(new_data);
        res.locals.message = Messages.SUCCESSFULLY_RECORD_ADDED;
        JsonResponse.jsonSuccess(req, res, this.url+'.addB');
    }

    /**
     * edit
     * @param req
     * @param res
     */
    public editB = async (req: Request, res: Response): Promise<void> => {
        const edit_data = req.body;
        edit_data.updated_by = req.body.logginedUser.user_id;
        res.locals.data = await this.modelRepo.edit(edit_data);
        if(res.locals.data){
            res.locals.message = Messages.SUCCESSFULLY_RECORD_UPDATED;
        }else{
            res.locals.message = Messages.INVALID_UPDATING;
        }
        JsonResponse.jsonSuccess(req, res, this.url+'.editB');
    }

    /**
     * delete
     * @param req
     * @param res
     */
    public deleteByIdB = async (req: Request, res: Response): Promise<void> => {
        const id: string = req.query[this.queryParam];
        if(id){
            const delete_id: any = await this.modelRepo.delete(id);
            res.locals.data = delete_id;
            res.locals.message = Messages.SUCCESSFULLY_RECEIVED;
        }else {
            res.locals.data = 0;
            res.locals.message = Messages.INVALID_DELETE;
        }
        JsonResponse.jsonSuccess(req, res, this.url+'.deleteByIdB');
    }

    /**
     * get by id
     * @param req
     * @param res
     */
    public getByIdB = async (req: Request, res: Response): Promise<void> => {
        const id: number = req.body[this.queryParam];
        console.log(req.body[this.queryParam]);
        const data: object = await this.modelRepo.getByID(id);
        res.locals.data = data;
        res.locals.message = Messages.SUCCESSFULLY_RECEIVED;
        JsonResponse.jsonSuccess(req, res, this.url+'.getByIdB');
    }

}