import { Document,Schema, model } from 'mongoose';

const CommentSchema = new Schema(
    {
        comment:{
            type: String,
            required: true
        },
        created_by: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        },
        question: {
            type: Schema.Types.ObjectId,
            ref: 'question'
        }
    },
    {
       timestamps: true,
       useNestedStrict: true
    }
);



export default model ('comment', CommentSchema);