import Comment from "./comment.model";
import {Messages} from "../../constants/messages";
import {BaseRepo} from "../BaseRepo";

export class CommentRepo extends BaseRepo{

    constructor() {
        super(Comment,'_id');
    }

}
