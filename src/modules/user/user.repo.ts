import User from "./user.model";
import {Messages} from "../../constants/messages";
import { IUser} from "./user.types";
import {BaseRepo} from "../BaseRepo";

export class UserRepo extends BaseRepo{

    constructor() {
        super(User,'_id');
    }

    public async getUsersProfile (params: IUser): Promise<any>{
        let users = await User.findOne({
                mobile_number: params.mobile_number
        });
        // console.log(users);
        let result = {
            success: true,
            data: users,
            messages: Messages.SUCCESSFULLY_RECEIVED
        };
        return result;
    }

    public async getUsersByMobileNo (mobile_number: number): Promise<any>{
        let users = await User.findOne({
            mobile_number: mobile_number
        }).populate('member_type_max').populate('member_type_min');
        return users;
    }

    public async getByID(id: number): Promise<object>{
        let data = await this.modelName.findById(id).populate('member_type_max').populate('member_type_min').exec();
        return Promise.resolve(data);

    }

    /*public loginValidation(req: Request, res: Response): Promise<ISuccessResponse> {
        var obj:ISuccessResponse = {
            success: false,
            message: "",
            data: null
        };
        var bodySchemaLogin = joi.object().keys({
            user_email: joi.string().email().required(),
            user_password: joi.string().required()
        });
        let bodySchema = bodySchemaLogin.validate(req.body,{abortEarly: false}, (err, value) => {
            if (err !== null) {
                obj.success = false;
                obj.data = {
                    validation: false,
                    isValid: false,
                    validation_error: err.details,
                };
                obj.message = Messages.VALIDATION_ERROR;
                return obj;
            }else{
                obj.success = true;
                obj.data = {
                    validation: true,
                    isValid: true
                };
                obj.message = Messages.SUCCESSFULLY_RECEIVED;
                return obj;
            }
        });
        return bodySchema;
    }*/

}
