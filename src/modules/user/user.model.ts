import { Document,Schema, model } from 'mongoose';

const UserSchema = new Schema(
    {
        name: {
            type: String,
            required: true,
            trim: true
        },
        mobile_number: {
            type: Number,
            required: true
        },
        email: {
            type: String,
            unique: true,
            match: /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/,
            required: true,
            trim: true
        },
        pin_code:{
            type: Number,
            required: true
        },
        profile_image: {
            type: String
        },
        doc_img: {
            type: String
        },
        doc: {
            type: String
        },
        //1 = admin , 2 = dealer , 3 = plumber
        user_type:{
            type: Number,
            required: true
        },
        dealer_code:{
            type: String
        },
        address: {
            type: String
        },
        dob:{
            type: Date
        },
        wedding_date: {
            type: Date
        },
        member_type_name_min:{
            type: String
        },
        member_type_min:{
            type: Schema.Types.ObjectId,
            ref: 'member_type'
        },
        member_type_name_max:{
            type: String
        },
        member_type_max:{
            type: Schema.Types.ObjectId,
            ref: 'member_type'
        },
        current_points:{
            type: Number
        },
        total_points:{
            type: Number
        },
        total_redemption:{
            type: Number
        }
    },
    {
        timestamps: true,
        useNestedStrict: true
    }
);



export default model ('User', UserSchema);