export interface IUser {
    _id?: any;
    email: string;
    password?: string;
    first_name?: string;
    last_name?: string;
    token?: string;
    mobile_number?: number;
}

export interface IUserRequest {
    email: string;
    password: string;
    first_name?: string;
    last_name?: string;
    user_token?: string;
    logginedUser?: IUser;
}

export interface IUserResponse {
    success: boolean;
    data: IUser;
    messages: string;
}