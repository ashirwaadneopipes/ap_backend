import * as joi from "joi";
import {Request, Response, NextFunction} from "express";
import {ISuccessValidation,JsonResponse} from "../../helper";
import {Messages} from "../../constants";



export class UserValidation {

    public async loginValidation(req: Request, res: Response, next:NextFunction): Promise<void> {
        console.log(req.body);
        var bodySchemaLogin = joi.object({
            mobile_number: joi.number().required().label('This field is invalid'),
            // password: joi.string().required().label('This field is invalid')
        });
        bodySchemaLogin.validate(req.body,{abortEarly: false},(err, value) => {
            if (err === null) {
                next();
            }else{
                res.locals.data ={
                    validation: false,
                    isValid: false,
                    validation_error: err.details,
                };
                JsonResponse.jsonSuccess(req, res);
            }
        });
    }

    public async addValidation(req: Request, res: Response, next:NextFunction): Promise<void> {
        console.log(req.body);
        var bodySchema = joi.object({
            email: joi.string().email().required().label('This field is invalid'),
            first_name : joi.string().required().label('This field is invalid'),
            last_name: joi.string().required().label('This field is invalid'),
            password: joi.string().required().label('This field is invalid')
        });
        bodySchema.validate(req.body,{abortEarly: false},(err, value) => {
            if (err === null) {
                next();
            }else{
                res.locals.data ={
                    validation: false,
                    isValid: false,
                    validation_error: err.details,
                };
                JsonResponse.jsonSuccess(req, res);
            }
        });
    }

    public async editValidation(req: Request, res: Response, next:NextFunction): Promise<void> {
        console.log(req.body);
        var bodySchema = joi.object({
            email: joi.string().email().required().label('This field is invalid'),
            _id : joi.string().required().label('This field is invalid'),
            first_name : joi.string().required().label('This field is invalid'),
            last_name: joi.string().required().label('This field is invalid'),
            // password: joi.string().required().label('This field is invalid')
        });
        bodySchema.validate(req.body,{abortEarly: false},(err, value) => {
            if (err === null) {
                next();
            }else{
                res.locals.data ={
                    validation: false,
                    isValid: false,
                    validation_error: err.details,
                };
                JsonResponse.jsonSuccess(req, res);
            }
        });
    }
}