import { Request, Response, Application } from "express";
import { BaseController } from '../BaseController';
import {UserRepo} from "./user.repo";
import { TryCatch,JsonResponse,AuthHelper,Upload } from "../../helper";
import {Messages} from "../../constants/messages";
import {UserValidation} from "./user.validation";
import {Environment} from "../../../config/environments/environment";
import {IUser} from "./user.types";

const SendOtp = require('sendotp');
import * as fs from 'fs';
import {MemberTypeRepo} from "../member-type/member-type.repo";

export class UserController extends BaseController {
    constructor(){
        super(new UserRepo(),'user','_id');
        this.init();
    }

    public register(express: Application) : void{
        express.use('/api/user', this.router);
    }

    public init(): void {

        const validation: UserValidation = new UserValidation();
        this.router.get('/',AuthHelper.guard ,TryCatch.tryCatchGlobe(this.indexB));
        //this.router.post('/', validation.addValidation, AuthHelper.guard, TryCatch.tryCatchGlobe(this.addB));
        this.router.put('/',  AuthHelper.guard, TryCatch.tryCatchGlobe(this.editB));
        this.router.delete('/', AuthHelper.guard, TryCatch.tryCatchGlobe(this.deleteByIdB));
        this.router.post('/get-by-id', AuthHelper.guard, TryCatch.tryCatchGlobe(this.getByIdB));

        this.router.post('/registration', /*validation.addValidation,*/ TryCatch.tryCatchGlobe(this.registration));
        this.router.post('/send-otp', TryCatch.tryCatchGlobe(this.sendOtp));
        this.router.post('/verify-otp', TryCatch.tryCatchGlobe(this.verifyOtp));
        this.router.post('/resend-otp', TryCatch.tryCatchGlobe(this.resendOtp));
        //this.router.post('/upload-img', TryCatch.tryCatchGlobe(this.uploadBaseFile));
    }

    /**
     * This is to List user
     * @param req
     * @param res
     */
    public async index(req: Request, res: Response): Promise<void> {
        const user_repo: UserRepo = new UserRepo();
        const user: object = await user_repo.index();
        res.locals.data = user;
        res.locals.message = Messages.SUCCESSFULLY_RECEIVED;
        JsonResponse.jsonSuccess(req, res, 'user.index');
    }

    /**
     * This is to registration user
     * @param req
     * @param res
     */
    public async registration(req: Request, res: Response): Promise<void> {
        const user_repo: UserRepo = new UserRepo();
        const member_repo = new MemberTypeRepo();
        const user_data = req.body;

        //profile image update
        if(user_data.profile_image){
            user_data.profile_image = await Upload.uploadBase64File("/uploads/user/profile",user_data.profile_image);
        }
        if(user_data.doc_img){
            user_data.doc_img = await Upload.uploadBase64File("/uploads/user/doc",user_data.doc_img);
        }
        user_data.member_type = 1;
        user_data.current_points = 0;
        user_data.total_points = 0;
        user_data.total_redemption = 0;

        //member type
        let membership_min:any = await member_repo.getMemberTypeMin(0);
        let membership_max:any = await member_repo.getMemberTypeMax(0)

        user_data.member_type_min = membership_min[0]._id;
        user_data.member_type_max = membership_max[0]._id;
        user_data.member_type_name_min = membership_min[0].type_name;
        user_data.member_type_name_max = membership_max[0].type_name;

        //member type



        const user:any = await user_repo.add(user_data);
        const authHelper: AuthHelper = new AuthHelper();
        const token: string = await authHelper.generateAuthToken(user);
        console.log("user_details:",user);
        console.log("user_token:",token);
        res.locals.data = {
            user_token : token,
            user_details: user
        };
        res.locals.message = Messages.SUCCESSFULLY_RECORD_ADDED;
        JsonResponse.jsonSuccess(req, res, 'user.registration');
    }

    public sendOtp(req: Request, res: Response){
        const env: Environment = new Environment();
        const mobile_number = req.body.mobile_number;
        const authKey: string = env.getConfiguration().otpConfig.authKey;
        const senderId: string = env.getConfiguration().otpConfig.senderId;
        const otpSms: string = env.getConfiguration().otpConfig.otpSms;
        const otpExpiry: number = env.getConfiguration().otpConfig.otpExpiry;
        const sendOtp = new SendOtp(authKey,otpSms);
        if(otpExpiry){
            sendOtp.setOtpExpiry(otpExpiry);
        }
        sendOtp.send(mobile_number, senderId, function (error:any, data:any) {
            if(error){
                res.locals.data = {
                    isVaild: false,
                    authorizationFailed: true
                };
                res.locals.message = error;
                JsonResponse.jsonError(req, res , 'user.sendOtp');
            }
            if(data.type == 'success'){
                res.locals.data = {mobile_number: mobile_number};
                res.locals.message = Messages.SUCCESSFULLY_OTP_SEND;
                JsonResponse.jsonSuccess(req, res, 'user.sendOtp');
            }
            /*else{
                res.locals.data = {mobile_number: mobile_number};
                res.locals.message = Messages.SUCCESSFULLY_OTP_SEND;
                JsonResponse.jsonSuccess(req, res, 'user.sendOtp');
            }*/
        });
    }

    public verifyOtp(req: Request, res: Response){
        const mobile_number = req.body.mobile_number;
        const otp_to_verify = req.body.otp_to_verify;
        const env: Environment = new Environment();
        const authKey: string = env.getConfiguration().otpConfig.authKey;
        const otpSms: string = env.getConfiguration().otpConfig.otpSms;
        const sendOtp = new SendOtp(authKey,otpSms);
        sendOtp.verify(mobile_number, otp_to_verify, async function (error:any, data:any) {
            if(error){
                res.locals.data = {
                    isVaild: false,
                    authorizationFailed: true
                };
                res.locals.message = error;
                JsonResponse.jsonError(req, res , 'user.verifyOtp');
            }
            if(data.type == 'success') {
                const user_repo: UserRepo = new UserRepo();
                const user = await user_repo.getUsersByMobileNo(mobile_number);
                if(user === null){
                    res.locals.data = {mobile_number: mobile_number , is_exist: false};
                }else{
                    const authHelper: AuthHelper = new AuthHelper();
                    const token: string = await authHelper.generateAuthToken(user);
                    // console.log("user_details:",user);
                    // console.log("user_token:",token);
                    res.locals.data = {
                        user_token : token,
                        user_details: user,
                        mobile_number: mobile_number ,
                        is_exist: true
                    };
                }
                res.locals.message = Messages.SUCCESSFULLY_OTP_VERIFIED;
                JsonResponse.jsonSuccess(req, res, 'user.verifyOtp');
            }
            if(data.type == 'error') {
                res.locals.success = false;
                res.locals.data = {mobile_number: mobile_number};
                res.locals.message = Messages.OTP_VERIFICATION_FAILED;
                JsonResponse.jsonSuccess(req, res, 'user.verifyOtp');
            }
        });
    }

    public resendOtp(req: Request, res: Response){
        const env: Environment = new Environment();
        const mobile_number = req.body.mobile_number;
        const authKey: string = env.getConfiguration().otpConfig.authKey;
        const otpSms: string = env.getConfiguration().otpConfig.otpSms;
        const otpExpiry: number = env.getConfiguration().otpConfig.otpExpiry;
        const sendOtp = new SendOtp(authKey,otpSms);
        if(otpExpiry){
            sendOtp.setOtpExpiry(otpExpiry);
        }
        sendOtp.retry(mobile_number, false, function (error:any, data:any) {
            if(error){
                res.locals.data = {
                    isVaild: false,
                    authorizationFailed: true
                };
                res.locals.message = error;
                JsonResponse.jsonError(req, res , 'user.sendOtp');
            }
            if(data.type == 'success'){
                res.locals.data = {mobile_number: mobile_number};
                res.locals.message = Messages.SUCCESSFULLY_OTP_RESEND;
                JsonResponse.jsonSuccess(req, res, 'user.sendOtp');
            }
        });
    }

    /**
     * get by id
     * @param req
     * @param res
     */
    public async getById(req: Request, res: Response) {
        const id: number = req.body._id;
        const user_repo: UserRepo = new UserRepo();
        const data: object = await user_repo.getByID(id);
        res.locals.success = false;
        res.locals.data = data;
        res.locals.message = Messages.SUCCESSFULLY_RECEIVED;
        JsonResponse.jsonSuccess(req, res, this.url+'.getByIdB');
    }

    /*public async uploadBaseFile(image_in_base64:any){
        const base = 'data:image/png;base64,'+image_in_base64;
        const convert_img = base.split(';base64,').pop();
        fs.writeFile('i.png',convert_img,{encoding: 'base64'},function (err) {
            console.log("done");
        });
    }*/

}