import { Document,Schema, model } from 'mongoose';

const QuestionSchema = new Schema(
    {
        question:{
            type: String,
            required: true
        },
        description:{
            type: String,
        },
        question_image:{
            type: String,
        },
        language:{
            type: String,
        },
        region:{
            type: String,
        },
        category:{
            type: String,
        },
        created_by: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        }
    },
    {
       timestamps: true,
       useNestedStrict: true,
        toJSON: { virtuals: true }
    }
);

QuestionSchema.virtual('comments', {
    ref: 'comment',
    localField: '_id',
    foreignField: 'question'
})



export default model ('question', QuestionSchema);