import Question from "./questions.model";
import {Messages} from "../../constants/messages";
import {BaseRepo} from "../BaseRepo";

export class QuestionsRepo extends BaseRepo{

    constructor() {
        super(Question,'_id');
    }

    public async index(relation: object = [],select_column: any = ''): Promise<object>{
        let data = await Question.find().populate('created_by').populate({
            path: 'comments',
            populate: {
                path: 'created_by'
            }
        }).exec();
        return Promise.resolve(data);
    }

    public async getByID(id: number): Promise<object>{
        let data = await this.modelName.findById(id).populate('created_by').populate({
            path: 'comments',
            populate: {
                path: 'created_by'
            }
        }).exec();
        return Promise.resolve(data);

    }

}
